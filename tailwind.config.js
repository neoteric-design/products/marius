module.exports = {
    plugins: [
        require('/../usr/local/lib/node_modules/@tailwindcss/forms'),
      ],

  purge: [
    './static/**/*.html',
    './static/**/*.js',
    './layouts/**/*.html',
    './layouts/*.html',
    './_vendor/**/**/*.html'
  ],
  theme: {
    extend: {
      colors: {
        primary: "var(--primary-color)", 
        secondary: "var(--secondary-color)", 
      },
      margin:{
        "2/3": "66.6666666%",
        "1/2":'50%',
        "2/5":'40%',
        "1/3":'33%',
        "1/4":'25%',
      },
      screens:{
        "xs":"480px",
        'print': {'raw': 'print'},
      },
      width:{
        "1/2":"50%",
        "55":"55%",
        "3/5":"60%"
      },
      maxWidth:{
        "1.5xl":"1440px",
        "80" : "80%"
      },
      minHeight:{
        "250":"250px"
      }
    }
  }
}
