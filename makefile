#all funcs

#run hugo server
hugo:
	hugo server --disableFastRender

dev:
	@NODE_ENV=development \
	make build


prod:
	@NODE_ENV=production \
	make build


#Run Tailwind build with npx. Find unprocessed.css and output styles.css
build:
	make sync
	tailwindcss build static/css/unprocessed.css -o static/css/styles.css


sync:
	git pull origin master
