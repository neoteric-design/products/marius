window.addEventListener('DOMContentLoaded', (event) => {
  ///////////////////////////////////////////////////////////////////////////FAQs Block
  let faqs = document.querySelectorAll(".faqs");

  faqs.forEach(faq => {
    faq.addEventListener("click", function(){
      faq.classList.contains("close") ? faq.classList.replace("close", "open") : faq.classList.replace("open", "close")
    })
  })

  const navLinks = document.querySelectorAll("nav .nav-links");
 
  for (const link of navLinks) {
    link.addEventListener("click", clickHandler);
  }
  
  function clickHandler(e) {
    e.preventDefault();
    const href = this.getAttribute("href");
  
    document.querySelector(href).scrollIntoView({
      behavior: "smooth"
    });
  }

  let updatesContainer = document.querySelectorAll('.updates-container-height');
    updatesContainer.forEach(c => {
      let childrenHeight = c.children[0].offsetHeight + c.children[1].offsetHeight;
      c.style.maxHeight = childrenHeight + "px";
    })
});

  ///////////////////////////////////////////////////////////////////////////Updates Block
  function toggleUpdates(btn) {
    let updates = document.querySelector('#updates-container');

    updates.classList.toggle('updates-container-height');
    if(updates.classList.contains("updates-container-height")){
      let childrenHeight = updates.children[0].offsetHeight + updates.children[1].offsetHeight;
      updates.style.maxHeight = childrenHeight + "px";
      console.log("childrenHeight", childrenHeight)
    }else{
      updates.style.maxHeight = "2400px";
    }

    if(btn.textContent == "See all Updates"){
      btn.textContent = "See less";
    }else{
      btn.textContent = "See all Updates";
    }
  }