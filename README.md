# Marius

Neoteric Design's Hugo theme for product websites.


## Setup

To add Marius to a project, insert

![Screen_Shot_2021-05-21_at_2.20.29_PM](/uploads/c01c27cd6e82d21a0454bb4f97efbbc0/Screen_Shot_2021-05-21_at_2.20.29_PM.png)

into the `config.toml`.

In your teminal, run:

`hugo mod get`

Afterwards, run `hugo server` to start the project.

To load Marius's JavaScript, add the following lines to your project's `config.toml`:

![Screen_Shot_2021-05-21_at_2.02.37_PM](/uploads/ed9a4f0bb5b2ecf6492b7e040ca9ff9f/Screen_Shot_2021-05-21_at_2.02.37_PM.png)

This will add functionality for the FAQs block, along with 'Smooth Scroll' for the primary navigation.


## Blocks and Forestry

As of now, Marius contains 5 blocks:
* Hero
* Feature grid with green checkmarks
* FAQs
* Alternating Images with Text
* CTA

Colors and font styles can be changed within the config stylesheet explained below.

Forestry templates must be contained at the project level. Copy the Forestry templates from [here](https://gitlab.com/neoteric-design/products/log-my-water/-/tree/master/.forestry/front_matter/templates), and add it to the root of your project. (Note: files will be added to Entrepot in the near future)



## Overrides

Out of the box, Marius comes with default styles for color and type. To override these, copy the `config.css` file from Marius' `static/css/`, and insert it into your project's `static/css` folder.

You are able to override:
* Header font
* Body font
* primary color
* secondary color


## Navigation

Marius is equipped to handle links to 4 components in the navigation. The name of the navigation links are arbitrary, and are configured in the `config.toml` like so:

![Screen_Shot_2021-05-21_at_1.49.27_PM](/uploads/67c0ba61fbadefeedbcaf5132738ea12/Screen_Shot_2021-05-21_at_1.49.27_PM.png)


As seen above, the links point to the `id` of the block components. As of now, the `id` of the block components, along with the corresponding link `url`s cannot be changed on a per project basis. Only the link `names` are configurable in the `config.toml`.

### Logo and Favicon

The logo in the primary navigation along with the favicon are both configurable.

Add the images into your project's `static/images` folder, and update the pathing in the `config.toml` like so:

![Screen_Shot_2021-05-21_at_2.13.58_PM](/uploads/dcdf9357513e4d97475974102d47831d/Screen_Shot_2021-05-21_at_2.13.58_PM.png)


### Analytics

Set one of: `.Site.Params.googleTagManager` or `.Site.Params.googleSiteTag` in config. In production mode on non-hidden pages, it is triggered from `partials/site/scripts-heads.html`
